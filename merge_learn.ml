(* ocamlopt -o Merge_learn str.cmxa unix.cmxa nums.cmxa merge_learn.ml *)

(* ***** author and license ***** *)
(* written by Lionel Tabourier, code under the terms of the Creative Commons 4.0 CC-BY License *)


Random.self_init ();;

let ios x = int_of_string x;;
let soi x = string_of_int x;;

(* ***** input definitions ***** *) 

let in_num_nodes = (ios Sys.argv.(1));;
let in_num_pred = (ios Sys.argv.(2));;
let in_g = (ios Sys.argv.(3));;
let in_num_ranking = (ios Sys.argv.(4));;
let in_list_ranking = Sys.argv.(5);;
let in_external_key = Sys.argv.(6);;


(* ***** general tools ***** *)

(* max_list : List[int] -> (int,int) *)
(* max_list lst : yields ( maximum value , position of the maximum value ) of lst *)
let max_list lst =
  let max = ref (0,0) in
  let memory_max_pos = ref [] in
  let counter = ref 0 in
  List.iter (fun x -> 
    if (x > (fst !max)) 
    then 
      begin
	max := (x , !counter);
	memory_max_pos := [!counter];
      end
    else if (x == (fst !max))
    then
      begin
	max := (x , !counter);
	memory_max_pos := !counter :: !memory_max_pos;
      end;
    counter := !counter +1; 
  ) lst;
  if (List.length !memory_max_pos) > 1
  then
    begin
      let ran = List.nth !memory_max_pos (Random.int (List.length !memory_max_pos)) in
      max := (fst !max , ran);
    end;
  !max;;

(* insert_list : List[alpha] alpha int -> List[alpha] *)
(* insert_list lst elt pos : yields list lst with element elt in position pos *)
let insert_list lst elt pos =
  let counter = ref 0 in
  let new_lst = ref [] in
  List.iter (fun x -> 
    begin
      if (!counter <> pos)
      then new_lst := x :: !new_lst
      else new_lst := elt :: !new_lst;
      counter := !counter +1
    end) lst;
  List.rev !new_lst;;

(* take_away : alpha List[alpha] -> List[alpha] *)
(* take_away elt lst : yields list lst without (the first occurrence) of element elt *)
let rec take_away elt = function
  | [] -> []
  | t :: q -> if t = elt then q else t :: (take_away elt q);;

(* print_output_list : stream lst -> () *)
(* print_output_list output_channel lst : prints list lst in stream output_channel *)
let rec print_output_list output_channel = function
  | [] -> ()
  | x :: l -> 
    begin 
      output_string output_channel (soi x); 
      output_string output_channel "\t";
      print_output_list output_channel l; 
    end;;

(* print_output_table : stream Table[int][int] -> () *)
(* print_output_table output_channel arr : prints table arr in stream output_channel *)
let print_output_table output_channel arr = 
  Array.iter (fun x -> 
    begin
      Array.iter (fun y ->
	begin 
	  output_string output_channel (soi y);
	  output_string output_channel "\t";
	end) x;
      output_string output_channel "\n";
    end) arr;;


(* ***** reading files ***** *)

(* checker : string -> bool *)
(* checker s : yields true when string s is not "" *)
let checker s = (s <> "" || s = "");;

(* max_line : string -> int *)
(* max_line name : yields the number of non-empty lines in the file name *)
let max_line name =
  let data = open_in name in
  let maximum = ref 0 in
  begin
    try
      while (checker (input_line data) = true) do
	maximum := !maximum +1;
      done;
    with | End_of_file -> close_in data;
  end;
  !maximum;;

(* rank_reader : string -> List[string] *)
(* rank_reader s : yields a list of strings corresponding to the content of s with a split at each ' ' or '\t' character *)
let rank_reader s =
  let r = Str.regexp "[ \t]+" in
  let l = Str.split r s in
    l;;

(* list_arg_reader : string -> List[string] *)
(* list_arg_reader s : yields a list of strings corresponding to the content of s with a split at each ',' character *)
let list_arg_reader s =
  let r = Str.regexp "[,]+" in
  let l = Str.split r s in
  l;;

(* ranking_maker : string int -> List[Table[int]] *)
(* ranking_maker names num_nodes : yields a list of tables, each table containing 3 columns corresponding to fields number 1, 2 and 3 contained in the files which names are separated with ',' in names , fields number 1 and 2 must be in the range [0:num_nodes-1] *)
let ranking_maker names num_nodes =
  let name_list = list_arg_reader names in
  let num_ranking = List.length name_list in
  let ranking_list = ref [] in
  for i=0 to num_ranking-1
  do
    let name_i = List.nth name_list i in
    let length_ranking = max_line name_i in
    let ranking_i = Array.make_matrix length_ranking 3 0 in
    let data_i = open_in name_i in
    let rank_i = ref 0 in
    begin
      try
	while true do
	  begin
            let line_i = rank_reader (input_line data_i) in
	    let node_1_i = (ios (List.nth line_i 0)) in
	    let node_2_i = (ios (List.nth line_i 1)) in
	    let score_i = (ios (List.nth line_i 2)) in
	    ranking_i.(!rank_i).(0) <- node_1_i;
 	    ranking_i.(!rank_i).(1) <- node_2_i;
 	    ranking_i.(!rank_i).(2) <- score_i;
	    rank_i := !rank_i +1;
	  end
	done
      with | End_of_file -> close_in data_i;
    end;
    ranking_list := ranking_i :: !ranking_list;
  done;
  List.rev (!ranking_list);; 



(* ***** ranking merging ***** *)

let merging ranking_list g num_pred num_nodes num_ranking external_key =
  let mixed_ranking = Array.make_matrix num_pred 3 0 in
  let counter_ranking = ref 0 in
  let ranked_ael = Array.make num_nodes [] in (* ranked_ael is adjacent edge list storing links already in the output *)
  let output_name = "./learning_"^external_key^".txt" in
  let output_channel = open_out output_name in
  let criterium_stop = ref [] in

  (* *** initialization *** *)

  (* scores initialization *)
  let sumscore_ranking_mixing = ref 0 in
  let sumscore_list = ref [] in
  for i=1 to num_ranking
  do 
    sumscore_list := 0 :: !sumscore_list;
    criterium_stop := false :: !criterium_stop;
  done;

  (* sliding index initialization *)
  let index_start_list = ref [] in
  let index_end_list = ref [] in
  for i=1 to num_ranking
  do 
    index_start_list := 0 :: !index_start_list;
    index_end_list := (g-1) :: !index_end_list;
  done;

  (* windows and qualities initialization *)
  let window_list = ref [] in
  for i=1 to num_ranking
  do 
    let window_i = Array.make num_nodes [] in
    window_list := window_i :: !window_list 
  done;
  let quality_list = ref [] in
  for i=1 to num_ranking
  do 
    quality_list := 0 :: !quality_list
  done;
  for i=0 to (num_ranking-1)
  do
    let ranking_i = List.nth ranking_list i in
    let window_i = List.nth !window_list i in
    let quality_i = ref (List.nth !quality_list i) in
    for j=0 to (g-1)
    do
      let node_1_ri = ranking_i.(j).(0) in
      let node_2_ri = ranking_i.(j).(1) in
      let score_ri = ranking_i.(j).(2) in 
      let inter_1_ri = window_i.(node_1_ri) in
      let inter_2_ri = window_i.(node_2_ri) in
      window_i.(node_1_ri) <- node_2_ri :: inter_1_ri;
      window_i.(node_2_ri) <- node_1_ri :: inter_2_ri;
      quality_i := !quality_i + score_ri;
    done;
    window_list := insert_list !window_list window_i i;
    quality_list := insert_list !quality_list !quality_i i;
  done;


  (* *** combining ranking *** *)

  let node_1 = ref 0 in
  let node_2 = ref 0 in
  let score = ref 0 in
  let category = ref 0 in

  while (!counter_ranking < (num_pred -1) && (List.mem false !criterium_stop)  == true)
  do
    begin

      (* ** selecting the category: the highest quality ranking ** *)

      let max_qual_pos = max_list !quality_list in
      category := (snd max_qual_pos);
      let ranking_i = (List.nth ranking_list !category) in
      let window_i = (List.nth !window_list !category) in
      let quality_i = ref (List.nth !quality_list !category) in
      let index_start_ri = ref (List.nth !index_start_list !category) in
      let index_end_ri = ref (List.nth !index_end_list !category) in
      
      (* ** updating the concerned category ** *)

      (* checking that the link is indeed in the window *)
      while (List.mem ranking_i.(!index_start_ri).(1) window_i.(ranking_i.(!index_start_ri).(0)) == false)
      do index_start_ri := !index_start_ri +1 done;

      (* defining the link taken out the window *)
      node_1 := ranking_i.(!index_start_ri).(0);
      node_2 := ranking_i.(!index_start_ri).(1);
	  
      (* upadating slinding index and quality of the window *)
      score := ranking_i.(!index_start_ri).(2);
      index_start_ri := !index_start_ri +1;
      index_end_ri := !index_end_ri +1;
      while (!index_end_ri < (Array.length ranking_i -1) &&
		List.mem  ranking_i.(!index_end_ri).(1) ranked_ael.(ranking_i.(!index_end_ri).(0)) == true)
      do index_end_ri := !index_end_ri +1 done;
      if (!index_start_ri == (Array.length ranking_i -1) || !index_end_ri == Array.length ranking_i -1) 
      then 
	begin
	  criterium_stop := insert_list !criterium_stop true !category;
	  quality_i := (-1);
	  quality_list := insert_list !quality_list !quality_i !category;
	end
      else
	begin
	  index_start_list := insert_list !index_start_list !index_start_ri !category;
	  index_end_list := insert_list !index_end_list !index_end_ri !category;

	  (* defining the link entering the window *)
	  let node_1_bis = ranking_i.(!index_end_ri).(0) in
	  let node_2_bis = ranking_i.(!index_end_ri).(1) in
	  let score_bis = ranking_i.(!index_end_ri).(2) in
	  quality_i := !quality_i - !score + score_bis;
	  quality_list := insert_list !quality_list !quality_i !category;
	  
	  (* updating the window of the concerned category *)
	  let inter_1_suppr = window_i.(!node_1) in
	  let inter_2_suppr = window_i.(!node_2) in
	  window_i.(!node_1) <- take_away !node_2 inter_1_suppr;
	  window_i.(!node_2) <- take_away !node_1 inter_2_suppr;
	  let inter_1_add = window_i.(node_1_bis) in
	  let inter_2_add = window_i.(node_2_bis) in
	  window_i.(node_1_bis) <- node_2_bis :: inter_1_add;
	  window_i.(node_2_bis) <- node_1_bis :: inter_2_add;
	  window_list := insert_list !window_list window_i !category;
	end;

      (* ** updating other windows and qualities ** *)

      for j=0 to (num_ranking -1)
      do
	if (j <> !category  && (List.nth !criterium_stop j == false))
	then
	  begin
	    let ranking_j = (List.nth ranking_list j) in
	    let window_j = (List.nth !window_list j) in
	    let quality_j = ref (List.nth !quality_list j) in
	    let index_start_rj = ref (List.nth !index_start_list j) in
	    let index_end_rj = ref (List.nth !index_end_list j) in
	    if (List.mem !node_1 window_j.(!node_2) == true)
	    then
	      begin

		(* updating sliding index, windows and qualities *)
		index_end_rj := !index_end_rj +1;
		if (!index_start_rj == (Array.length ranking_j -1) || !index_end_rj == Array.length ranking_j)
		then
		  begin
		    criterium_stop := insert_list !criterium_stop true j;
		    quality_j := (-1);
		    quality_list := insert_list !quality_list !quality_j j;
		  end
		else
		  begin
		    while (!index_end_rj < (Array.length ranking_j -1) 
			   && List.mem ranking_j.(!index_end_rj).(1) ranked_ael.(ranking_j.(!index_end_rj).(0)) == true)
		    do index_end_rj := !index_end_rj +1 done;
		    if (!index_start_rj == (Array.length ranking_j -1) || !index_end_rj == Array.length ranking_j -1) 
		    then
		      begin 
			criterium_stop := insert_list !criterium_stop true j;
			quality_j := (-1);
			quality_list := insert_list !quality_list !quality_j j;
		      end
		    else
		      begin
			index_end_list := insert_list !index_end_list !index_end_rj j;
			let node_1_bis = ranking_j.(!index_end_rj).(0) in
			let node_2_bis = ranking_j.(!index_end_rj).(1) in
			let score_bis = ranking_j.(!index_end_rj).(2) in
			let inter_1_suppr = window_j.(!node_1) in
			let inter_2_suppr = window_j.(!node_2) in
			window_j.(!node_1) <- take_away !node_2 inter_1_suppr;
			window_j.(!node_2) <- take_away !node_1 inter_2_suppr;
			let inter_1_add = window_j.(node_1_bis) in
			let inter_2_add = window_j.(node_2_bis) in
			window_j.(node_1_bis) <- node_2_bis :: inter_1_add;
			window_j.(node_2_bis) <- node_1_bis :: inter_2_add;
			window_list := insert_list !window_list window_j j;
			quality_j := !quality_j - !score + score_bis;
			quality_list := insert_list !quality_list !quality_j j;
		      end;
		  end
	      end
	  end
      done;

      (* ** updating ranked_ael and output ranking ** *)

      if (List.mem !node_2 ranked_ael.(!node_1) == false)
      then
	begin

	  (* updating ranked_ael *)
	  let inter_1 = ranked_ael.(!node_1) in
	  ranked_ael.(!node_1) <- !node_2 :: inter_1;
	  let inter_2 = ranked_ael.(!node_2) in
	  ranked_ael.(!node_2) <- !node_1 :: inter_2;
	  
	  (* updating mixed_ranking and counter_ranking *)
	  mixed_ranking.(!counter_ranking).(0) <- !node_1;
	  mixed_ranking.(!counter_ranking).(1) <- !node_2;
	  mixed_ranking.(!counter_ranking).(2) <- !score;

	  (* updating sumscores *)
	  let inter_sumscore = ref [] in
	  for j=0 to num_ranking-1
	  do
	    let ranking_j = (List.nth ranking_list j) in
	    if (List.nth !criterium_stop j == false && !counter_ranking < Array.length ranking_j)
	    then 
	      begin
		let inter_score = List.nth !sumscore_list j in 
		inter_sumscore := ((inter_score) + ranking_j.(!counter_ranking).(2)) :: !inter_sumscore;
	      end
	    else
	      begin
		inter_sumscore := 0 :: !inter_sumscore;
		criterium_stop := insert_list !criterium_stop true j;
	      end
	  done;
	  sumscore_list := (List.rev !inter_sumscore);
	  sumscore_ranking_mixing := !sumscore_ranking_mixing + mixed_ranking.(!counter_ranking).(2);
	  counter_ranking := !counter_ranking +1;
	end;
      
      (* ** writing learning file ** *)
      
      if (!counter_ranking mod 1 == 0)
      then
	begin
	  print_output_list output_channel !index_start_list;
	  output_string output_channel "\t";
	  print_output_list output_channel !sumscore_list;
	  output_string output_channel "\t";
	  output_string output_channel (soi !sumscore_ranking_mixing);
	  output_string output_channel "\n";
	end;

    end
  done;

  close_out output_channel;
  mixed_ranking;;


(* ***** execution ***** *)

let my_list_ranking = ranking_maker in_list_ranking in_num_nodes;;

let my_mixed_ranking = merging my_list_ranking in_g in_num_pred in_num_nodes in_num_ranking in_external_key;;

let output_ranking_name = "./ranking_"^in_external_key^".txt" in
let output_ranking_channel = open_out output_ranking_name in
print_output_table output_ranking_channel my_mixed_ranking;
close_out output_ranking_channel;;

