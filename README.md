### Summary

RankMerging is a supervised method for learning-to-rank, especially designed for link prediction problems in large and sparse networks. 

### Content

+ Two source codes (.ml):

	- merge_learn.ml: source code of the learning phase of RankMerging
	- merge_test.ml: source code of the test phase of RankMerging


+ Two rankings for learning (r1_learning.txt and r2_learning.txt) and the two corresponding rankings for test (r1_testing.txt and r2_testing.txt).

### About the source code

The source codes in the archive are provided in OCaml, if you are interested in this language, go to https://ocaml.org/

### Author and licence

These programs have been written by Lionel Tabourier.

These programs are free softwares: you can redistribute it and/or modify them under the terms of the Creative Commons 4.0 CC-BY License. 

For more details, see http://creativecommons.org.au/licences.

### Reference

If you use these codes, please cite:

Tabourier, L., Bernardes, D. F., Libert, A. S., & Lambiotte, R. (2019). RankMerging: a supervised learning-to-rank framework to predict links in large social networks. Machine Learning, 108(10), 1729-1756.
